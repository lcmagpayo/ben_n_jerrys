from .models import Flavor, Ingredient, SourcingValue
from rest_framework import serializers

class FlavorSerializer(serializers.ModelSerializer):
    allergy_info = serializers.CharField(required=False, allow_blank=True, max_length=500)
    dietary_certifications = serializers.CharField(required=False, allow_blank=True, max_length=100)
    ingredients = serializers.SerializerMethodField()
    sourcing_values = serializers.SerializerMethodField()

    def get_ingredients(self, obj):
        return obj.ingredients.values_list('ingredient', flat=True)

    def get_sourcing_values(self, obj):
        return obj.sourcing_values.values_list('sourcing_value', flat=True)

    def create_or_update_ingredient_list(self, data, flavor):
        ingredient_list = data.get('ingredients', [])
        for ingredient in ingredient_list:
            Ingredient.objects.create(flavor=flavor, ingredient=ingredient)

    def create_or_update_svalue_list(self, data, flavor):
        svalue_list = data.get('sourcing_values', [])
        for svalue in svalue_list:
            SourcingValue.objects.create(flavor=flavor, sourcing_value=svalue)

    def create(self, validated_data):
        data = self.context['request'].data.copy()
        flavor = Flavor.objects.create(**validated_data)
        self.create_or_update_ingredient_list(data, flavor)
        self.create_or_update_svalue_list(data, flavor)
        return flavor

    def update(self, instance, validated_data):
        data = self.context['request'].data.copy()

        instance.productId = validated_data.get('productId', instance.productId )
        instance.name = validated_data.get('name', instance.name )
        instance.image_closed = validated_data.get('image_closed', instance.image_closed )
        instance.image_open= validated_data.get('image_open', instance.image_open)
        instance.description = validated_data.get('description', instance.description )
        instance.story = validated_data.get('story', instance.story )
        instance.allergy_info = validated_data.get('allergy_info', instance.allergy_info)
        instance.dietary_certifications = validated_data.get('dietary_certifications', instance.dietary_certifications )
        
        # Handle updating ingredient list
        Ingredient.objects.filter(flavor_id=instance.productId).delete()
        flavor = Flavor.objects.get(productId=instance.productId)
        self.create_or_update_ingredient_list(data, flavor)

        # Handle updating sourcing value list
        SourcingValue.objects.filter(flavor_id=instance.productId).delete()
        flavor = Flavor.objects.get(productId=instance.productId)
        self.create_or_update_svalue_list(data, flavor)
        
        return instance

    class Meta:
        model = Flavor
        fields = ('productId', 'name', 'image_closed', 'image_open', 'description', 'story', 'sourcing_values', 'ingredients', 'allergy_info', 'dietary_certifications')