# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from .models import Flavor, Ingredient, SourcingValue
from rest_framework import generics
from rest_framework import viewsets
from .serializers import FlavorSerializer
from rest_framework.permissions import IsAdminUser

class FlavorList(generics.ListAPIView):
    """
    Read-only API endpoint that allows flavors to be viewed
    """
    queryset = Flavor.objects.all()
    serializer_class = FlavorSerializer

    def get_queryset(self):
        """
        Optionally restricts the returned list by an ingredient or
        sourcing value by filtering against a query parameter in the URL.
        """
        queryset = Flavor.objects.all()
        ingredient = self.request.query_params.get('ingredient', None)
        svalue = self.request.query_params.get('sourcing_value', None)
        if ingredient is not None:
            flavor_ids = Ingredient.objects.values_list('flavor_id').filter(ingredient=ingredient)
            queryset = Flavor.objects.all().filter(productId=flavor_ids)
        if svalue is not None:
            flavor_ids = SourcingValue.objects.values_list('flavor_id').filter(sourcing_value=svalue)
            queryset = Flavor.objects.all().filter(productId=flavor_ids)
        return queryset

class FlavorDetail(generics.RetrieveAPIView):

    queryset = Flavor.objects.all()
    serializer_class = FlavorSerializer

class AdminViewSet(viewsets.ModelViewSet):
    """
    API endpoint for Admin user to edit data
    """

    queryset = Flavor.objects.all()
    serializer_class = FlavorSerializer
    permission_classes = (IsAdminUser,)
