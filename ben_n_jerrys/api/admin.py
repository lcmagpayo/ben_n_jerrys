# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Flavor, Ingredient, SourcingValue

admin.site.register(Flavor)
admin.site.register(Ingredient)
admin.site.register(SourcingValue)

# Register your models here.