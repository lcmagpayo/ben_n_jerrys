from django.utils.six import BytesIO
from .models import Flavor, Ingredient, SourcingValue
from rest_framework.parsers import JSONParser
from rest_framework import serializers

class FlavorSerializer(serializers.ModelSerializer):
    allergy_info = serializers.CharField(required=False, allow_blank=True, max_length=500)
    dietary_certifications = serializers.CharField(required=False, allow_blank=True, max_length=100)

    class Meta:
        model = Flavor
        fields = ('productId', 'name', 'image_closed', 'image_open', 'description', 'story', 'allergy_info', 'dietary_certifications')

def json_to_db(data_file):
    file_object = open(data_file, 'r')
    json = file_object.read()
    stream = BytesIO(json)
    data = JSONParser().parse(stream)

    for flavor in data:
        flavor['productId']=int(flavor['productId'])
        flavor_serializer = FlavorSerializer(data=flavor)
        if flavor_serializer.is_valid():
            flavor_serializer.save()
        else:
            print flavor_serializer.errors
        flavor_name = flavor['name']
        for ingredient in flavor['ingredients']:
            ingredient = Ingredient(flavor=Flavor.objects.get(name=flavor_name), ingredient=ingredient)
            ingredient.save()
        for sourcing_value in flavor['sourcing_values']:
            svalue = SourcingValue(flavor=Flavor.objects.get(name=flavor_name), sourcing_value=sourcing_value)
            svalue.save()
    file_object.close()