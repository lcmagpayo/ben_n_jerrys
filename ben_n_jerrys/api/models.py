# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Flavor(models.Model):
    def __str__(self):
        return self.name
    productId = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=100)
    image_closed = models.CharField(max_length=500)
    image_open = models.CharField(max_length=500)
    description = models.CharField(max_length=500)
    story = models.CharField(max_length=500)
    allergy_info = models.CharField(max_length=500)
    dietary_certifications = models.CharField(max_length=100)

# From the ingredients array
class Ingredient(models.Model):
    flavor = models.ForeignKey(Flavor, related_name='ingredients', on_delete=models.CASCADE)
    ingredient = models.CharField(max_length=100)
    def __unicode__(self):
        return self.ingredient

# From the sourcing_values array
class SourcingValue(models.Model):
    flavor = models.ForeignKey(Flavor, related_name='sourcing_values', on_delete=models.CASCADE)
    sourcing_value = models.CharField(max_length=100)
    def __unicode__(self):
        return self.sourcing_value