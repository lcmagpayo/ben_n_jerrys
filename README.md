# Ben-n-Jerrys-API

## Installation
- Install django https://docs.djangoproject.com/en/2.0/topics/install/
- `pip install django`
- Install djang rest framework http://www.django-rest-framework.org/#installation
- `pip install djangorestframework`

### Database Creation
- cd to ben_n_jerrys
- `python manage.py makemigrations`

## Populating the Database
- Per Django documentation, it is recommended to populate the database using migrations.
- In the automatically created migration file in the previous step, copy the contents of
	api/migrations/load_initial_data.py and add it to the migration file
- `python manage.py migrate`
- This should automatically create the tables in the database "db.sqlite3"

### Create Admin User
- python manage.py createsuperuser --email admin@example.com --username admin

## Running the Server
- `python manage.py runserver`
-  Local server should be available at http://127.0.0.1:8000/

## Accessing the API
- The API is accessible at http://localhost:3000/api/

## Available Routes

- Get list of all flavors
  - GET `/api/flavors`
- Get information on a specific flavor based on its productId
  - GET `/api/flavors/2190`
- Get list of all flavors which has a specific ingredient
  - GET `/api/v1/flavors?ingredient=milk`
- Get list of all flavors which has a sourcing value
  - GET `/api/v1/flavors?sourcing_value=non-gmo`

### For Admin User

- only admin user can modify data
- /api/admin/
    - GET, PUT, PATCH, CREATE, DELETE